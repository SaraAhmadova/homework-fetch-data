const moviesContainer = document.getElementById("movies-container");

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((movies) => {
    movies.forEach((movie) => {
      const movieElement = document.createElement("div");
      movieElement.classList.add("movie");
      movieElement.innerHTML = `
            <h3>Episode ${movie.episodeId}: ${movie.name}</h3>
            <div class="characters-list" id="characters-${movie.episodeId}"></div>
            <div class="loading" id="loading-${movie.episodeId}">Loading...</div>
            <p>${movie.openingCrawl}</p>
          `;
      moviesContainer.appendChild(movieElement);
      fetchCharacters(movie.characters, movie.episodeId);
    });
  })
  .catch((error) => console.error("Error fetching movies:", error));

function fetchCharacters(characterUrls, episodeId) {
  const charactersContainer = document.getElementById(
    `characters-${episodeId}`
  );
  const loadingElement = document.getElementById(`loading-${episodeId}`);

  loadingElement.style.display = "block";

  Promise.all(
    characterUrls.map((url) => fetch(url).then((response) => response.json()))
  )
    .then((characters) => {
      charactersContainer.innerHTML = `<strong>Characters:</strong> ${characters
        .map((character) => character.name)
        .join(", ")}`;
    })
    .catch((error) =>
      console.log(`Error fetching characters for Episode ${episodeId}:`, error)
    )
    .finally(() => {
      loadingElement.style.display = "none";
    });
}
